package db

import (
	"fmt"
	"os"

	_ "database/sql"

	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/natajdacoliveira/dissertando-back-end/logger"
)

// TODO: test logging data after
// TODO: maybe check the singleton instance aswell
func InitDb() *sqlx.DB {
	err := godotenv.Load(".env")
	if err != nil {
		logger.NewLogger().Error(err.Error())
		return nil
	}

	db, err := sqlx.Connect("postgres",
		fmt.Sprintf(`
			host=%v 
			user=%v 
			password=%v 
			dbname=%v port=%v 
			sslmode=%v`,
			os.Getenv("DB_HOST"),
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASSWORD"),
			os.Getenv("DB_NAME"),
			os.Getenv("DB_PORT"),
			os.Getenv("DB_SSL"),
		))

	if err != nil {
		logger.NewLogger().Error(err.Error())
		return nil
	}

	return db
}
