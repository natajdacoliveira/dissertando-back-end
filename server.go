package main

import (
	"fmt"
	"os"

	"github.com/getsentry/sentry-go"
	sentryecho "github.com/getsentry/sentry-go/echo"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/natajdacoliveira/dissertando-back-end/logger"
	"gitlab.com/natajdacoliveira/dissertando-back-end/routes"
)

func main() {
	InitializeSentry()
	InitializeAPI()
}

func InitializeAPI() {
	file, err := os.OpenFile("storage/api.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(fmt.Sprintf("Error at opening log file: %v", err))
	}
	defer file.Close()

	API := echo.New()
	API.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:9000"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
	}))

	API.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Output: file,
	}))
	API.Use(middleware.Recover())
	API.Use(sentryecho.New(sentryecho.Options{}))

	routes.Routes(API)

	API.Logger.Fatal(API.Start("127.0.0.1:3000"))
}

func InitializeSentry() {
	err := godotenv.Load(".env")
	if err != nil {
		err = fmt.Errorf("error at loading envinronment variables: %w; ", err)
		logger.NewLogger().Error(err.Error())
	}

	if err := sentry.Init(sentry.ClientOptions{
		Dsn:              os.Getenv("SENTRY_DSN"),
		TracesSampleRate: 1.0,
	}); err != nil {
		err = fmt.Errorf("erro ao inicializar Sentry: %w; ", err)
		logger.NewLogger().Error(err.Error())
	}
}
