package auth

import (
	"gitlab.com/natajdacoliveira/dissertando-back-end/db"
	"gitlab.com/natajdacoliveira/dissertando-back-end/models"
)

func saveRefreshToken(refreshToken models.RefreshToken) error {
	db := db.InitDb()
	defer db.Close()

	_, err := db.NamedExec(`
		INSERT INTO 
			refresh_token 
				(token,
				expires_at,
				created_at,
				updated_at,
				app_user_uuid)
			VALUES
				(:token, 
				:expires_at, 
				:created_at, 
				:updated_at, 
				:app_user_uuid)
	`, refreshToken)
	if err != nil {
		return err
	}

	return nil
}

func revokeRefreshToken(appUserUuid string) {
	db := db.InitDb()
	defer db.Close()

	db.MustExec(`
		UPDATE
			refresh_token
		SET
			is_revoked = true,
			updated_at = NOW()
		WHERE
			app_user_uuid = $1
	`, appUserUuid)
}

func useRefreshToken(appUserUuid string) error {
	db := db.InitDb()
	defer db.Close()

	_, err := db.NamedExec(`
		UPDATE
			refresh_token
		SET
			used_at = NOW(),
			updated_at = NOW()
		WHERE 
			app_user_uuid = $1
	`, appUserUuid)
	if err != nil {
		return err
	}

	return nil
}
