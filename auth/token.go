package auth

import (
	"net/http"
	"os"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/natajdacoliveira/dissertando-back-end/models"
)

func NewAccessToken(claims models.UserClaims) (string, error) {
	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return accessToken.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
}

func NewRefreshToken(claims jwt.StandardClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return "", err
	}

	refreshToken := models.RefreshToken{
		Token:     tokenString,
		ExpiresAt: time.Unix(claims.ExpiresAt, 0),
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		IsRevoked: false,
		UsedAt:    nil,
		UserUUID:  claims.Issuer,
	}

	err = saveRefreshToken(refreshToken)
	if err != nil {
		return "", err
	}

	return token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
}

func ParseAccessToken(accessToken string) (*models.UserClaims, error) {
	parsedAccessToken, err := jwt.ParseWithClaims(accessToken, &models.UserClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("TOKEN_SECRET")), nil
	})
	if err != nil {
		return nil, err
	}

	return parsedAccessToken.Claims.(*models.UserClaims), nil
}

func ParseRefreshToken(refreshToken string) *jwt.StandardClaims {
	parsedRefreshToken, _ := jwt.ParseWithClaims(refreshToken, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("TOKEN_SECRET")), nil
	})
	return parsedRefreshToken.Claims.(*jwt.StandardClaims)
}

func RotateRefreshToken(refreshToken string) (string, error) {
	claims := ParseRefreshToken(refreshToken)
	RevokeRefreshToken(claims.Issuer)

	refreshToken, err := NewRefreshToken(*claims)
	if err != nil {
		return "", err
	}

	return refreshToken, nil
}

func UseRefreshToken(refreshToken string) error {
	claims := ParseRefreshToken(refreshToken)
	err := useRefreshToken(claims.Issuer)
	if err != nil {
		return err
	}

	return nil
}

func RevokeRefreshToken(appUserUuid string) {
	revokeRefreshToken(appUserUuid)
}

func SetRefreshTokenCookie(refreshToken string, ctx echo.Context) {
	cookie := new(http.Cookie)
	cookie.Name = "refreshToken"
	cookie.Value = refreshToken
	cookie.Expires = time.Unix(ParseRefreshToken(refreshToken).ExpiresAt, 0)
	cookie.HttpOnly = true
	cookie.Secure = true
	cookie.SameSite = http.SameSiteStrictMode

	ctx.SetCookie(cookie)
}
