package models

import (
	"time"

	"github.com/golang-jwt/jwt"
)

type UserClaims struct {
	UUID  string `json:"uuid" db:"uuid"`
	Email string `json:"email" db:"email"`
	jwt.StandardClaims
}

type Login struct {
	Email    string `json:"email" form:"email" query:"email" valid:"type(string),required,email"`
	Password string `json:"password" form:"password" query:"password" valid:"type(string),required"`
}

type RefreshToken struct {
	UUID      string     `json:"uuid" db:"uuid"`
	Token     string     `json:"token" db:"token"`
	IsRevoked bool       `json:"isrevoked" db:"is_revoked"`
	ExpiresAt time.Time  `json:"expires_at" db:"expires_at"`
	CreatedAt time.Time  `json:"created_at" db:"created_at"`
	UpdatedAt time.Time  `json:"updated_at" db:"updated_at"`
	UsedAt    *time.Time `json:"used_at" db:"used_at"`
	UserUUID  string     `json:"app_user_uuid" db:"app_user_uuid"`
}
