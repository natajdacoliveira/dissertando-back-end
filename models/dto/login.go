package dto

import "gitlab.com/natajdacoliveira/dissertando-back-end/models"

type LoginBodyDTO struct {
	Email    string `json:"email" form:"email" query:"email" valid:"type(string),required,email"`
	Password string `json:"password" form:"password" query:"password" valid:"type(string),required"`
}

type LoginResDTO struct {
	Data struct {
		User        models.User `json:"user"`
		AccessToken string      `json:"accessToken"`
	} `json:"data"`
}
