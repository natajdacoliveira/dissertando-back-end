package models

import "time"

type User struct {
	UUID            string     `json:"uuid" db:"uuid"`
	Name            *string    `json:"name" db:"name"`
	Password        string     `json:"-" db:"password"`
	Email           string     `json:"email" db:"email"`
	IsActive        bool       `json:"isActive" db:"is_active"`
	BornAt          *time.Time `json:"bornAt" db:"born_at"`
	Bio             *string    `json:"bio" db:"bio"`
	Avatar          *string    `json:"avatar" db:"avatar"`
	IsFaceVerified  bool       `json:"isFaceVerified" db:"is_face_verified"`
	IsEmailVerified bool       `json:"isEmailVerified" db:"is_email_verified"`
	EmailVerifyCode *string    `json:"-" db:"email_verify_code"`
	CreatedAt       *time.Time `json:"createdAt" db:"created_at"`
	UpdatedAt       *time.Time `json:"updatedAt" db:"updated_at"`
	DeletedAt       *time.Time `json:"deletedAt" db:"deleted_at"`
}
