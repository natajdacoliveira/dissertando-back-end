package routes

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/natajdacoliveira/dissertando-back-end/modules/user"
)

func Routes(e *echo.Echo) {
	userRouts(e)
}

func userRouts(e *echo.Echo) {
	u := e.Group("/users")

	u.POST("/login", user.LoginHandler)
	u.GET("/login/access-token", user.LoginAccessTokenHandler)
}
