package handler

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func BaseHandler(ctx echo.Context) error {
	return ctx.String(http.StatusOK, "Hello, World!")
}

func BaseWithAuthTestHandler(ctx echo.Context) error {
	// token := ctx.Get("token").((*auth.Token))
	return echo.NewHTTPError(http.StatusUnauthorized, "Erro de autenticação")
}
