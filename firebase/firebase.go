package firebase

import (
	"context"
	"net/http"
	"strings"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/natajdacoliveira/dissertando-back-end/logger"
)

func FirebaseApp() (*firebase.App, error) {
	app, err := firebase.NewApp(context.Background(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "could not initialize firebaseApp")
	}
	return app, nil
}

func FirebaseAuth() (*auth.Client, error) {
	app, err := FirebaseApp()
	if err != nil {
		return nil, err
	}
	auth, err := app.Auth(context.Background())
	if err != nil {
		return nil, errors.Wrap(err, "could not initialize firebaseAuth")
	}
	return auth, nil
}

func FirebaseMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		auth, err := FirebaseAuth()
		if err != nil {
			logger.NewLogger().Error(err.Error())
			return echo.NewHTTPError(http.StatusInternalServerError, "Erro interno, tente novamente mais tarde")
		}

		authFromRequest := ctx.Request().Header.Get("Authorization")
		idToken := strings.Replace(authFromRequest, "Bearer ", "", 1)
		token, err := auth.VerifyIDToken(context.Background(), idToken)
		if err != nil {
			logger.NewLogger().Error(err.Error())
			return echo.NewHTTPError(http.StatusUnauthorized, "Erro de autenticação")
		}

		ctx.Set("token", token)
		return next(ctx)
	}
}
