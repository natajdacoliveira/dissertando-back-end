include .env
export

MIGRATE=sql-migrate

limit=1
name=default

migrate-status:
	$(MIGRATE) status -env=production

migrate-up:
	$(MIGRATE) up -limit=${limit} -env=production

migrate-down:
	$(MIGRATE) down -limit=${limit} -env=production

migrate-redo:
	$(MIGRATE) redo -env=production

migrate-create:
	$(MIGRATE) new -env=production ${name}

migrate-skip:
	$(MIGRATE) skip -env=production

migrate-ud:
	$(MIGRATE) up -limit=${limit} -env=production
	$(MIGRATE) down -limit=${limit} -env=production

migrate-du:
	$(MIGRATE) down -limit=${limit} -env=production
	$(MIGRATE) up -limit=${limit} -env=production

.PHONY: migrate-status migrate-up migrate-down migrate-redo migrate-create migrate-skip migrate-du migrate-ud