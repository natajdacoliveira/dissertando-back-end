-- +migrate Up
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE app_user (
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL UNIQUE,
  password TEXT NOT NULL,
  is_active BOOLEAN NOT NULL DEFAULT TRUE,
  born_at DATE,
  bio TEXT,
  avatar TEXT,
  is_face_verified BOOLEAN NOT NULL DEFAULT FALSE,
  is_email_verified BOOLEAN NOT NULL DEFAULT FALSE,
  email_verify_code VARCHAR(20),
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TABLE app_log (
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  content JSON NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE
);

CREATE TABLE refresh_token (
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  token TEXT NOT NULL,
  is_revoked BOOLEAN NOT NULL DEFAULT FALSE,
  expires_at TIMESTAMP NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  used_at TIMESTAMP,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE
);

CREATE TABLE essay_theme(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TABLE essay_theme_support_data(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  essay_theme_uuid UUID NOT NULL,
  content JSON NOT NULL,
  image TEXT,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (essay_theme_uuid) REFERENCES essay_theme(uuid) ON DELETE CASCADE
);

CREATE TABLE essay_status(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE essay(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  writer_uuid UUID NOT NULL,
  essay_theme_uuid UUID NOT NULL,
  base_essay UUID,
  essay_status_uuid UUID NOT NULL,
  title VARCHAR(255) NOT NULL,
  content JSON NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (base_essay) REFERENCES essay(uuid) ON DELETE CASCADE,
  FOREIGN KEY (essay_theme_uuid) REFERENCES essay_theme(uuid) ON DELETE CASCADE,
  FOREIGN KEY (essay_status_uuid) REFERENCES essay_status(uuid) ON DELETE CASCADE,
  FOREIGN KEY (writer_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE
);

CREATE TABLE essay_rating(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  reviewer_uuid UUID NOT NULL,
  essay_uuid UUID NOT NULL,
  rating INTEGER NOT NULL DEFAULT 0,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (essay_uuid) REFERENCES essay(uuid) ON DELETE CASCADE,
  FOREIGN KEY (reviewer_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE
);

CREATE TABLE essay_review(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  reviewer_uuid UUID NOT NULL,
  essay_uuid UUID NOT NULL,
  score INTEGER NOT NULL DEFAULT 0,
  content JSON NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (reviewer_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (essay_uuid) REFERENCES essay(uuid) ON DELETE CASCADE
);

CREATE TABLE app_profile(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  is_active BOOLEAN NOT NULL DEFAULT TRUE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TABLE permission(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  is_active BOOLEAN NOT NULL DEFAULT TRUE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TABLE app_profile_permission(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_profile_uuid UUID NOT NULL,
  permission_uuid UUID NOT NULL,
  is_active BOOLEAN NOT NULL DEFAULT TRUE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (app_profile_uuid) REFERENCES app_profile(uuid) ON DELETE CASCADE,
  FOREIGN KEY (permission_uuid) REFERENCES permission(uuid) ON DELETE CASCADE
);

CREATE TABLE app_user_permission(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  permission_uuid UUID NOT NULL,
  is_active BOOLEAN NOT NULL DEFAULT TRUE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (permission_uuid) REFERENCES permission(uuid) ON DELETE CASCADE
);

CREATE TABLE app_user_app_profile(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  app_profile_uuid UUID NOT NULL,
  valid_until TIMESTAMP,
  is_active BOOLEAN NOT NULL DEFAULT TRUE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (app_profile_uuid) REFERENCES app_profile(uuid) ON DELETE CASCADE
);

CREATE TABLE friend(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  friend_uuid UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (friend_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE
);

CREATE TABLE friend_request(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  sender_uuid UUID NOT NULL,
  receiver_uuid UUID NOT NULL,
  note VARCHAR(255),
  is_accepted BOOLEAN NOT NULL DEFAULT FALSE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (sender_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (receiver_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE
);

CREATE TABLE party(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  description TEXT,
  image TEXT,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TABLE party_invitation(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  party_uuid UUID NOT NULL,
  receiver_uuid UUID NOT NULL,
  sender_uuid UUID NOT NULL,
  is_accepted BOOLEAN NOT NULL DEFAULT FALSE,
  note VARCHAR(255),
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (receiver_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (sender_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (party_uuid) REFERENCES party(uuid) ON DELETE CASCADE
);

CREATE TABLE party_app_user(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  party_uuid UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (party_uuid) REFERENCES party(uuid) ON DELETE CASCADE
);

CREATE TABLE party_app_user_permission(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  party_uuid UUID NOT NULL,
  permission_uuid UUID NOT NULL,
  is_active BOOLEAN NOT NULL DEFAULT TRUE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (party_uuid) REFERENCES party(uuid) ON DELETE CASCADE,
  FOREIGN KEY (permission_uuid) REFERENCES permission(uuid) ON DELETE CASCADE
);

CREATE TABLE essay_font_size(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  size INTEGER NOT NULL DEFAULT 16,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE essay_comment(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  essay_uuid UUID NOT NULL,
  content JSON NOT NULL,
  position JSON NOT NULL,
  is_done BOOLEAN NOT NULL DEFAULT FALSE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (essay_uuid) REFERENCES essay(uuid) ON DELETE CASCADE
);

CREATE TABLE essay_comment_answer(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  essay_comment_uuid UUID NOT NULL,
  content JSON NOT NULL,
  essay_comment_answer_uuid UUID,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (essay_comment_answer_uuid) REFERENCES essay_comment_answer(uuid) ON DELETE CASCADE,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE,
  FOREIGN KEY (essay_comment_uuid) REFERENCES essay_comment(uuid) ON DELETE CASCADE
);

CREATE TABLE character_font_size(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  character CHAR(1) NOT NULL,
  width_centimeters INTEGER NOT NULL DEFAULT 1,
  height_centimeters INTEGER NOT NULL DEFAULT 1,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE character_font_size_preset(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE character_font_size_preset_character_font_size(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  character_font_size_preset_uuid UUID NOT NULL,
  character_font_size_uuid UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (character_font_size_preset_uuid) REFERENCES character_font_size_preset(uuid) ON DELETE CASCADE,
  FOREIGN KEY (character_font_size_uuid) REFERENCES character_font_size(uuid) ON DELETE CASCADE
);

CREATE TABLE user_character_font_size(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  character_font_size_uuid UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (character_font_size_uuid) REFERENCES character_font_size(uuid) ON DELETE CASCADE,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE
);

CREATE TABLE app_user_setting(
  uuid UUID DEFAULT gen_random_uuid() PRIMARY KEY,
  app_user_uuid UUID NOT NULL,
  language VARCHAR(255) NOT NULL DEFAULT 'pt-br',
  theme VARCHAR(255) NOT NULL DEFAULT 'dark',
  essay_font_size_uuid UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  FOREIGN KEY (essay_font_size_uuid) REFERENCES essay_font_size(uuid) ON DELETE CASCADE,
  FOREIGN KEY (app_user_uuid) REFERENCES app_user(uuid) ON DELETE CASCADE
);

INSERT INTO
  app_user (uuid, name, email, password)
VALUES
  (
    uuid_generate_v4(),
    'admin',
    'natajdacoliveira@gmail.com',
    crypt(
      '123456' || 'WwHGn2bMb&@CzU85KauM&DE!%EeQsfz4^ZbuxgmmYjUnGzon8hA7589DNinG%Kts%NMrL#Lf&HzFi8gtjk$QXq*A$6bhzWAK9ar72b^@T8&CmbQ#BPJi^sVLjSes@jWG',
      gen_salt('bf', 8)
    )
  );

INSERT INTO
  essay_status (uuid, name)
VALUES
  (uuid_generate_v4(), 'draft');

INSERT INTO
  essay_status (uuid, name)
VALUES
  (uuid_generate_v4(), 'review');

INSERT INTO
  essay_status (uuid, name)
VALUES
  (uuid_generate_v4(), 'rejected');

INSERT INTO
  essay_status (uuid, name)
VALUES
  (uuid_generate_v4(), 'approved');

INSERT INTO
  app_profile (uuid, name)
VALUES
  (uuid_generate_v4(), 'admin');

INSERT INTO
  app_profile (uuid, name)
VALUES
  (uuid_generate_v4(), 'student');

INSERT INTO
  app_profile (uuid, name)
VALUES
  (uuid_generate_v4(), 'teacher');

INSERT INTO
  app_profile(uuid, name)
VALUES
  (uuid_generate_v4(), 'party_admin');

INSERT INTO
  app_profile(uuid, name)
VALUES
  (uuid_generate_v4(), 'party_member');

INSERT INTO
  app_user_app_profile (uuid, app_user_uuid, app_profile_uuid)
VALUES
  (
    uuid_generate_v4(),
    (
      SELECT
        uuid
      FROM
        app_user
      WHERE
        email = 'natajdacoliveira@gmail.com'
    ),
    (
      SELECT
        uuid
      FROM
        app_profile
      WHERE
        name = 'admin'
    )
  );

INSERT INTO
  app_user_app_profile (uuid, app_user_uuid, app_profile_uuid)
VALUES
  (
    uuid_generate_v4(),
    (
      SELECT
        uuid
      FROM
        app_user
      WHERE
        email = 'natajdacoliveira@gmail.com'
    ),
    (
      SELECT
        uuid
      FROM
        app_profile
      WHERE
        name = 'student'
    )
  );

INSERT INTO
  app_user_app_profile (uuid, app_user_uuid, app_profile_uuid)
VALUES
  (
    uuid_generate_v4(),
    (
      SELECT
        uuid
      FROM
        app_user
      WHERE
        email = 'natajdacoliveira@gmail.com'
    ),
    (
      SELECT
        uuid
      FROM
        app_profile
      WHERE
        name = 'teacher'
    )
  );

INSERT INTO
  essay_font_size (uuid, size)
VALUES
  (uuid_generate_v4(), 16);

INSERT INTO
  essay_font_size (uuid, size)
VALUES
  (uuid_generate_v4(), 18);

INSERT INTO
  essay_font_size (uuid, size)
VALUES
  (uuid_generate_v4(), 20);

-- +migrate Down
DROP TABLE IF EXISTS app_user_app_profile;

DROP TABLE IF EXISTS refresh_token;

DROP TABLE IF EXISTS app_user_setting;

DROP TABLE IF EXISTS essay_font_size;

DROP TABLE IF EXISTS essay_rating;

DROP TABLE IF EXISTS essay_comment_answer;

DROP TABLE IF EXISTS essay_theme_support_data;

DROP TABLE IF EXISTS essay_review;

DROP TABLE IF EXISTS user_character_font_size;

DROP TABLE IF EXISTS character_font_size_preset_character_font_size;

DROP TABLE IF EXISTS character_font_size_preset;

DROP TABLE IF EXISTS essay_comment;

DROP TABLE IF EXISTS character_font_size;

DROP TABLE IF EXISTS essay;

DROP TABLE IF EXISTS essay_status;

DROP TABLE IF EXISTS essay_theme;

DROP TABLE IF EXISTS app_log;

DROP TABLE IF EXISTS friend_request;

DROP TABLE IF EXISTS friend;

DROP TABLE IF EXISTS party_app_user;

DROP TABLE IF EXISTS party_invitation;

DROP TABLE IF EXISTS party_app_user_permission;

DROP TABLE IF EXISTS app_profile_permission;

DROP TABLE IF EXISTS app_user_permission;

DROP TABLE IF EXISTS permission;

DROP TABLE IF EXISTS app_profile;

DROP TABLE IF EXISTS party;

DROP TABLE IF EXISTS app_user;

DROP EXTENSION IF EXISTS "pgcrypto";

DROP EXTENSION IF EXISTS "uuid-ossp";