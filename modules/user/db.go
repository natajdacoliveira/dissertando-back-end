package user

import (
	"os"

	"github.com/pkg/errors"
	"gitlab.com/natajdacoliveira/dissertando-back-end/db"
	models "gitlab.com/natajdacoliveira/dissertando-back-end/models"
	"golang.org/x/crypto/bcrypt"
)

func loginDb(login models.Login) (models.User, error) {
	db := db.InitDb()
	defer db.Close()

	user := models.User{}
	err := db.Get(&user,
		`
			SELECT
				*
			FROM app_user
			WHERE
				email = $1 
				AND 
				is_active = TRUE
		`,
		login.Email)
	if err != nil {
		return models.User{}, err
	}

	password := login.Password + os.Getenv("PEPPER_SECRET")
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return models.User{}, errors.Wrap(err, "credenciais inválidas")
	}

	return user, nil
}

func getUserByUUID(uuid string) (models.User, error) {
	db := db.InitDb()
	defer db.Close()

	user := models.User{}
	err := db.Get(&user,
		`
			SELECT
				*
			FROM app_user
			WHERE
				uuid = $1,
				AND
				is_active = TRUE
		`,
		uuid)
	if err != nil {
		return models.User{}, err
	}

	return user, nil
}
