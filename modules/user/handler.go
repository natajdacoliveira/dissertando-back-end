package user

import (
	"net/http"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/natajdacoliveira/dissertando-back-end/auth"
	"gitlab.com/natajdacoliveira/dissertando-back-end/logger"
	models "gitlab.com/natajdacoliveira/dissertando-back-end/models"
	"gitlab.com/natajdacoliveira/dissertando-back-end/models/dto"
	"gitlab.com/natajdacoliveira/dissertando-back-end/validate"
)

func LoginHandler(ctx echo.Context) error {
	login := new(models.Login)
	if err := ctx.Bind(login); err != nil {
		logger.NewLogger().Error(err.Error())
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	_, err := govalidator.ValidateStruct(login)
	if err != nil {
		logger.NewLogger().Error(err.Error())
		return ctx.JSON(http.StatusBadRequest, validate.ValidateErrorWrapper(err))
	}

	user, err := loginDb(*login)
	if err != nil {
		logger.NewLogger().Error(err.Error())
		return ctx.JSON(http.StatusUnauthorized, map[string]string{"errors": err.Error()})
	}

	auth.RevokeRefreshToken(user.UUID)

	twoDays := time.Hour * 48
	fiveMinutes := time.Minute * 5
	expiresAtRefreshToken := time.Now().Add(twoDays)
	expiresAtAccessToken := time.Now().Add(fiveMinutes)

	claimsToken := models.UserClaims{
		UUID:  user.UUID,
		Email: user.Email,
		StandardClaims: jwt.StandardClaims{
			Issuer:    user.UUID,
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: expiresAtAccessToken.Unix(),
		},
	}
	claimsRefreshToken := jwt.StandardClaims{Issuer: user.UUID, IssuedAt: time.Now().Unix(),
		ExpiresAt: expiresAtRefreshToken.Unix(),
	}

	accessToken, err := auth.NewAccessToken(claimsToken)
	if err != nil {
		logger.NewLogger().Error(err.Error())
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"errors": err.Error()})
	}

	refreshToken, err := auth.NewRefreshToken(claimsRefreshToken)
	if err != nil {
		logger.NewLogger().Error(err.Error())
		return ctx.JSON(http.StatusInternalServerError, map[string]string{"errors": err.Error()})
	}

	res := dto.LoginResDTO{}
	res.Data.User = user
	res.Data.AccessToken = accessToken

	auth.SetRefreshTokenCookie(refreshToken, ctx)

	return ctx.JSON(http.StatusOK, res)
}

func LoginAccessTokenHandler(ctx echo.Context) error {
	accessToken := ctx.Request().Header.Get("Authorization")[7:]

	refreshToken, err := ctx.Cookie("refreshToken")
	if err != nil {
		logger.NewLogger().Error(err.Error())
		return echo.NewHTTPError(http.StatusUnauthorized, "token inválido")
	}

	return echo.NewHTTPError(http.StatusUnauthorized, map[string]*http.Cookie{accessToken: refreshToken})
}
