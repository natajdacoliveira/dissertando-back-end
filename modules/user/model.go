package user

type User struct {
	UUID            string `json:"uuid" db:"uuid"`
	Name            string `json:"name" db:"name"`
	Email           string `json:"email" db:"email"`
	IsActive        bool   `json:"is_active" db:"is_active"`
	BornAt          string `json:"born_at" db:"born_at"`
	Bio             string `json:"bio" db:"bio"`
	Avatar          string `json:"avatar" db:"avatar"`
	IsFaceVerified  bool   `json:"is_face_verified" db:"is_face_verified"`
	IsEmailVerified bool   `json:"is_email_verified" db:"is_email_verified"`
	EmailVerifyCode string `json:"email_verify_code" db:"email_verify_code"`
	CreatedAt       string `json:"created_at" db:"created_at"`
	UpdatedAt       string `json:"updated_at" db:"updated_at"`
	DeletedAt       string `json:"deleted_at" db:"deleted_at"`
}
